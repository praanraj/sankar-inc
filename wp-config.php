<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sankarinc');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!tR-#)ejjRe RA&<ihn>Ko6j84dK6k;mqogvvtfr_L)%T0WA**anX [zgN*`#BP;');
define('SECURE_AUTH_KEY',  'a$1VMu((V8 kF)zfn)D=,[K+h*8iM]7aW<bp#a)uGWyFPi?0>K,D_uvm&IwCpW:*');
define('LOGGED_IN_KEY',    'cG2$-W1s?)aqc;5i`[S:OJWQ925~BkL>wwc&`EvFaMI]`y{[1eXWnsqw64t9&Y)0');
define('NONCE_KEY',        '@xq^lu;{bE>L[Jq{ %pv=pbYZ6=@+.UXTCodh3mx,Ch9,7(]LYmz(B3Q!%%JW:H;');
define('AUTH_SALT',        'ap+~oRht:H*g~w3nnYKv8tCIz,jx{v)RLcl 6t_v :.WRveZ.:ygHS}WCy,{``I-');
define('SECURE_AUTH_SALT', 'VFr[=@{/AI9)KlJf8$.roG6=FQxWwkMY|9_x!.r?^O]V;J 7~ou2!hqsXO!g5RRa');
define('LOGGED_IN_SALT',   '4%M;>DTnMJth^.*915!A?WB3] 4(**8?vaC!pdq0wWYHj4^_4X**IPDz#A8,:#BT');
define('NONCE_SALT',       'h%FVj6UI1y]P-$MxSfdQLtM`r@((x{]c~82fv0tK36T?1MW]S!YYb=(unmmFmmP8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
